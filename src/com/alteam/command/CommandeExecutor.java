package com.alteam.command;

import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public abstract class CommandeExecutor
{
	public abstract void execute(Player sender, String[] args);
	
	public abstract void executeByConsole(ConsoleCommandSender sender, String[] args);
	
}
