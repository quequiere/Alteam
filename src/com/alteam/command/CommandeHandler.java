package com.alteam.command;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.alteam.Alteam;
import com.altisplug.AltisPlug;

public class CommandeHandler implements CommandExecutor
{
	public CommandeHandler()
	{

	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String main, String[] args)
	{
		if (cmd.getName().equalsIgnoreCase("team"))
		{
		
			if (args.length == 0)
			{
				displayPossibilities(sender);
				return true;
			}
			else
			{
				try
				{
					CommandeTeam ct = CommandeTeam.valueOf(args[0]);
					
					if(ct==null)
					{
						sender.sendMessage(ChatColor.RED + "Commande non existante !");
						displayPossibilities(sender);
						return true;
					}
					else
					{
						if(ct.hasPermission(sender))
						{
							ct.execute(sender, args);
							return true;
						}
						else
						{
							sender.sendMessage(ChatColor.RED + "Vous ne disposez pas de cette commande ==> " + "alteam.commande."+args[0]);
							return true;
						}
						
					}
				}
				catch(IllegalArgumentException e)
				{
					sender.sendMessage(ChatColor.RED + "Commande non existante !");
					displayPossibilities(sender);
					return true;
				}
				
				
			}	
		}
		return true;
	}
	
	public static void displayPossibilities(CommandSender s)
	{
		s.sendMessage(ChatColor.BLUE+"-----------------");
		s.sendMessage(ChatColor.YELLOW+"Liste des commandes:");
		
		for(CommandeTeam ct:CommandeTeam.values())
		{
			if(ct.hasPermission(s))
			{
				s.sendMessage(ChatColor.GREEN+"- /alt "+ct.toString());
			}
			else
			{
				s.sendMessage(ChatColor.RED+"- /alt "+ct.toString());
			}
		}
		
		s.sendMessage(ChatColor.BLUE+"-----------------");
		
	}

}
