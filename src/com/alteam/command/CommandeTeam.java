package com.alteam.command;

import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import com.alteam.Alteam;
import com.alteam.command.executor.giveCharte;

public enum CommandeTeam
{
	givecharte(new giveCharte());

	private CommandeExecutor ce;

	CommandeTeam(CommandeExecutor ce)
	{
		this.ce = ce;
	}

	public void execute(CommandSender sender, String[] args)
	{
		if (sender instanceof Player)
		{
			Player p = (Player) sender;
			this.ce.execute(p, args);
		}
		else
		{
			this.ce.executeByConsole(((ConsoleCommandSender) sender), args);
		}

	}

	public boolean hasPermission(CommandSender sender)
	{
		if (Alteam.permission.has(sender, "alteam.commande." + this.toString()))
		{
			return true;
		}
		return false;
	}

}
