package com.alteam.command.executor;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.alteam.command.CommandeExecutor;

public class giveCharte extends CommandeExecutor
{

	@Override
	public void execute(Player p, String[] args)
	{
		this.giveCharte(p);
		p.sendMessage(ChatColor.GREEN+"Une charte vierge a été ajoutée à votre inventaire.");
	}

	@Override
	public void executeByConsole(ConsoleCommandSender sender, String[] args)
	{
		if(args.length==2)
		{
			Player p = Bukkit.getPlayer(args[1]);
			if(p!=null)
			{
				this.giveCharte(p);
			}
			else
			{
				sender.sendMessage(ChatColor.RED+"Impossible de trouver "+args[1]);
			}
		}
		else
		{
			sender.sendMessage(ChatColor.RED+"/team givecharte player");
		}
		
	}
	
	

	public static void giveCharte(Player p)
	{
		ItemStack item = new ItemStack(Material.PAPER);
		ItemMeta itemMeta = item.getItemMeta();
		itemMeta.setDisplayName(ChatColor.GREEN+"Charte non initialisée");
		ArrayList<String> lore = new ArrayList<String>();
		lore.add("Ceci est une charte non initialisée.");
		lore.add("Merci de la prendre et faire un click droit.");
		lore.add("Vous pourrez ainsi choisir le nom de votre clan.");
		lore.add("Il vous faudra 10 membres pour ouvrir le clan.");
		itemMeta.setLore(lore);
		item.setItemMeta(itemMeta);
		p.getInventory().addItem(item);
	}
	

}
