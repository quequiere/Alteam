package com.alteam;

import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.RegisteredServiceProvider;

import com.altisplug.util.qqcolor;
import com.earth2me.essentials.Essentials;

public class Config
{

	public static FileConfiguration config;

	public static void Init(FileConfiguration fileConfiguration, Alteam plug)
	{
		InitDependence(plug);
		
		config = fileConfiguration;
		config.options().header("Fichier de configuration pour le plugin AltisPlug par quequiere et Sancho");
		

		config.options().copyDefaults(true);
		plug.saveConfig();
	}
	
	public static void InitDependence(Alteam plug)
	{
		setupEconomy(plug);
		setupPermissions(plug);
		
		Essentials essp = (Essentials) plug.getServer().getPluginManager().getPlugin("Essentials");
		if (essp != null)
		{
			System.out.println(qqcolor.ANSI_GREEN + "------Initialisation essential OK -------" + qqcolor.ANSI_RESET);
			Alteam.essentials = essp;
		}
		else
		{
			System.out.println(qqcolor.ANSI_RED + "!!!!!!!!!!! Essentials non valide !!!!!!!!!" + qqcolor.ANSI_RESET);
		}
		
		//Pas besoin d'initialiser gson car AltisPlug le fait déjà normalement !
		
		/*
		System.out.println("Chargement des libs externes ( gson etc ...)");
		File s = new File("libs/gson.jar");
		try
		{
			ClassLoaderQuequiere.addClassPath(ClassLoaderQuequiere.getJarUrl(s));
		}
		catch (IOException e)
		{
			System.out.println(qqcolor.ANSI_RED + "Chargement error !!!!" + qqcolor.ANSI_RESET);
			e.printStackTrace();
		}*/
	}
	
	public static boolean setupEconomy(Alteam plug)
	{
		RegisteredServiceProvider<Economy> economyProvider = plug.getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
		if (economyProvider != null)
		{
			Alteam.economy = economyProvider.getProvider();
		}

		return (Alteam.economy != null);
	}

	public static boolean setupPermissions(Alteam plug)
	{
		RegisteredServiceProvider<Permission> permissionProvider = plug.getServer().getServicesManager().getRegistration(net.milkbowl.vault.permission.Permission.class);

		if (permissionProvider != null)
		{
			Alteam.permission = permissionProvider.getProvider();
		}

		return (Alteam.permission != null);
	}


}
