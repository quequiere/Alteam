package com.alteam;

import java.util.logging.Logger;

import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;

import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.alteam.command.CommandeHandler;
import com.earth2me.essentials.Essentials;



public class Alteam extends JavaPlugin
{
	public final static Logger log = Logger.getLogger("Minecraft");
	private static Plugin plugin;
	
	public static Permission permission = null;
	public static Economy economy = null;
	public static Essentials essentials = null;

	@Override
	public void onDisable()
	{
		log.info("AltisPlug disabled.");
	}

	@Override
	public void onEnable()
	{
		log.info("Loading Alteam");
		
		plugin = this;
		Config.Init(this.getConfig(), this);

		getCommand("team").setExecutor(new CommandeHandler());
	}

	public static Plugin getPlugin()
	{
		return plugin;
	}
}
