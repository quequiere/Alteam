package com.alteam.charte;

import java.util.ArrayList;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.altisplug.classes.superClasse.EnumClasses;

public class Charte
{
	private int id;
	private String name;
	private String president;
	private ArrayList<String> futurMembre;
	private EnumClasses classeType;

	public Charte(int id, String name,Player porigine,EnumClasses classe)
	{
		this.id = id;
		this.name = name;
		this.president=porigine.getName();
		this.classeType=classe;
		this.futurMembre=new ArrayList<String>();
	}

}
