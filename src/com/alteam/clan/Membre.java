package com.alteam.clan;

import com.alteam.exeption.NoPresidentExeption;

public class Membre
{
	private String name;
	private EnumGrade grade;
	private Clan clan;
	
	public Membre(String name,Clan clan,EnumGrade eg)
	{
		this.name=name;
		this.clan=clan;
		this.grade=eg;
	}
	
	
	public EnumGrade getGrade()
	{
		return this.grade;
	}
	
	public boolean isPresident()
	{
		try
		{
			if(this.clan.getPresident().equals(this))
			{
				return true;
			}
		}
		catch (NoPresidentExeption e)
		{
			e.printStackTrace();
		}
		
		return false;
	}
	
	public Clan getClan()
	{
		return this.clan;
	}
	
	

}
