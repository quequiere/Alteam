package com.alteam.clan;

import java.util.ArrayList;

import com.alteam.exeption.NoPresidentExeption;

public class Clan
{
	private String name;
	private ArrayList<Membre> membreListe;
	
	public Clan(String name,ArrayList<Membre> m)
	{
		this.name=name;
		this.membreListe = new ArrayList<Membre>();
		this.membreListe.addAll(m);
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public Membre getPresident() throws NoPresidentExeption
	{
		for(Membre m:this.membreListe)
		{
			if(m.getGrade().equals(EnumGrade.president))
			{
				return m;
			}
		}
		
		throw new NoPresidentExeption(this.getName());
	}

}
